import { React, useState, useEffect } from "react";
import './App.css';

const urlAPI = 'https://pixabay.com/api/?key='
const keyAPI = '38007170-606cd397a40590a152ce77011'
const reseachParam = "&q="
const perPage = "&per_page="

const choiceNumbers = [3, 5, 10, 50, 200]

function App() {

  const [research, setResearch] = useState("")
  const [numberOfImages, setNumberOfImages] = useState(3)
  const [foundedImage, setFoundedImage] = useState("")

  const fetchData = async () => {
    const result = await fetch(urlAPI.concat(keyAPI, research, perPage, numberOfImages))
    result.json().then(json => {
      setFoundedImage(json);
    })
  }

  const reusableFetch = (e) => {

    e.preventDefault()

    fetchData();
  };

  useEffect(() => {
    fetchData();
  }, [numberOfImages]);

  return (
    <div className="container mx-auto bg-rose-800 rounded-xl shadow p-8 m-10">
      <form onSubmit={e => reusableFetch(e)} className="flex">
        <div className=" bg-white rounded flex items-center w-full p-3 shadow-sm border border-gray-200">
          <input type="text" onChange={e => setResearch(reseachParam.concat(e.target.value).replace(" ", "+"))} className="w-full pl-4 text-sm outline-none focus:outline-none bg-transparent" />
          <button type="submit" className="outline-none focus:outline-none"><svg className=" w-5 text-gray-600 h-5 cursor-pointer" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" stroke="currentColor" viewBox="0 0 24 24"><path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg></button>
        </div>
        <select onChange={e => setNumberOfImages(e.target.value)} className="text-sm outline-none focus:outline-none bg-white rounded-xl">
          {choiceNumbers.map(value => <option value={value}>{value}</option>)}
        </select>
      </form>

      <section id="photos" class="my-5 grid grid-cols-1 md:grid-cols-4 gap-4">
        {foundedImage.hits?.map(value => <img src={value.largeImageURL} class="w-full h-64 object-cover" />)}
      </section>
    </div>
  );
}

export default App;
